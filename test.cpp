//
// Created by asar on 28.07.15.
//

#include <gtest/gtest.h>
#include <../BitFlag.hpp>
#include <iostream>

enum BigEnum
{
  BigEnum_0 = 0,
  BigEnum_1 = 1,
  BigEnum_2 = 2,
  BigEnum_3 = 3
};

enum threeBitEnum
{
  threeBitEnum_0 = 0,
  threeBitEnum_1 = 1,
  threeBitEnum_2 = 2,
  threeBitEnum_3 = 3,
  threeBitEnum_4 = 4
};

enum fourBitEnum
{
  fourBitEnum_0 = 0,
  fourBitEnum_1 = 1,
  fourBitEnum_2 = 2,
  fourBitEnum_3 = 3,
  fourBitEnum_4 = 4,
  fourBitEnum_5 = 5,
  fourBitEnum_6 = 6,
  fourBitEnum_7 = 7,
  fourBitEnum_8 = 8,
  fourBitEnum_9 = 9,
  fourBitEnum_10 = 10,
  fourBitEnum_11 = 11,
  fourBitEnum_12 = 12,
  fourBitEnum_13 = 13,
  fourBitEnum_14 = 14,
  fourBitEnum_15 = 15
};

TEST(BitFlags, simpleTest)
{
  BitFlags<BigEnum, 2, 44> x;

  x.setSate(2, BigEnum_3);
  x.setSate(30, BigEnum_2);

  ASSERT_TRUE(x.isFlagSate(2, BigEnum_3));
  ASSERT_FALSE(x.isFlagSate(2, BigEnum_2));
  ASSERT_TRUE(x.isFlagSate(20, BigEnum_0));
}


TEST(BitFlags, threeBitEnum)
{
  BitFlags<threeBitEnum, 3, 30> x;
  
  x.setSate(2, threeBitEnum_3);
  x.setSate(1, threeBitEnum_4);
  x.setSate(3, threeBitEnum_4);
  x.setSate(2, threeBitEnum_1);
  
  ASSERT_EQ(threeBitEnum_1, x.getState(2)) << x;
}

TEST(BitFlags, fourBitEnumToBig)
{
  BitFlags<fourBitEnum, 3, 30> x;
  x.setSate(2, fourBitEnum_15);
  ASSERT_EQ(fourBitEnum_0, x.getState(2)) << x;
}

TEST(BitFlags, fourBitEnumOutPofScope)
{
  BitFlags<fourBitEnum, 4, 30> x;
  x.setSate(32, fourBitEnum_15);
  ASSERT_EQ(fourBitEnum_0, x.getState(2)) << x;
}
