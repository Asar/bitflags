//
// Created by asar on 28.07.15.
//

#ifndef BITFLAGS_BITFLAG_HPP
#define BITFLAGS_BITFLAG_HPP


#include <ostream>
#include <bitset>
#include <cstdint>
#include <cstring>
#include <cmath>

typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t u8;

template <int size>
class BitMask
{
public:
  static const int mask = (1 << (size - 1)) | BitMask<size - 1>::mask;
};

template<>
class BitMask<1>
{
public:
  static const int mask = 1;
};

template<>
class BitMask<0>
{
public:
  static const int mask = 0;
};

template<int size, int maxAmountOfFlags>
class ArraySize
{
public:
  static const int max = (size * maxAmountOfFlags) / 8 + ( ((size * maxAmountOfFlags) % 8) != 0);
};

template<>
class ArraySize<0,0>
{
public:
  static const int max = 1;
};


template<class FlagType, int size, int maxAmountOfFlags>
class BitFlags
{
public:
  BitFlags()
  {
      std::memset(_flags, 0, sizeof(_flags));
  }

  bool isFlagSate(u32 pos, FlagType state) const
  {
      return (getState(pos) == state);
  }

  FlagType getState(u32 pos) const
  {
    position p = getPos(pos);
    return static_cast<FlagType>((_flags[p.array] >> (p.posInArray))  & BitMask<size>::mask );
  }
  
  void setSate(u32 pos, FlagType state)
  {
    if( state > static_cast<FlagType>(BitMask<size>::mask) || pos >= maxAmountOfFlags)
    {
      return;
    }
    position p = getPos(pos);
    _flags[p.array] = _flags[p.array] & ~(BitMask<size>::mask << (p.posInArray));
    _flags[p.array] = _flags[p.array] | (state << (p.posInArray));
  }

  template<class FlagType1, int size1, int maxAmountOfFlags1>
  friend std::ostream& operator<<(std::ostream& out, const BitFlags<FlagType1, size1, maxAmountOfFlags1>& x ) ;

private:
  u8 _flags[ArraySize<size, maxAmountOfFlags>::max];

  struct position
  {
    u8 array;
    u8 posInArray;
  };
  
  position getPos(u32 pos) const
  {
    position p;
    p.array = std::round((pos * size) / 8.0);
    p.posInArray = (pos % ArraySize<size, maxAmountOfFlags>::max) * size;
    return p;
  }
  

};

template<class FlagType, int size, int maxAmountOfFlags>
std::ostream& operator<<(std::ostream& out, const BitFlags<FlagType, size, maxAmountOfFlags>& x )
{
  for(int i = ArraySize<size, maxAmountOfFlags>::max - 1 ; i >= 0  ; --i)
  {
      std::bitset<8> a(x._flags[i]);
      for(int j = 8; j >= 0 ; --j)
      {
       out << a[j] << (j % size == 0 ? ".": "");
      }
      out << ",";
  }
  return out ;
}

#endif //BITFLAGS_BITFLAG_HPP
