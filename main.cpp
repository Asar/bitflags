// Example program
#include <iostream>
#include "BitFlag.hpp"

enum BigEnum
{
    BigEnum_0 = 0,
    BigEnum_1 = 1,
    BigEnum_2 = 2,
    BigEnum_3 = 3
};

int main()
{
    BitFlags<BigEnum, 2, 44> x;

    x.setSate(2, BigEnum_3);
    x.setSate(30, BigEnum_3);

    std::cout << x << ":" << sizeof(x) << std::endl;
//
//    int x = 5;
//    int* px = &x;
//
//    std::cout << x << ":" << px << ":" << *px << std::endl;

}